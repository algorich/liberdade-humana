# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Proposal do

    it { should_not have_valid(:content).when('', nil) }
    it { should have_valid(:content).when('some content', "some multiline\ncontent\n") }
    it { should have_valid(:proponent).when(build :proponent) }
    it { should have_valid(:edition).when(build :edition) }

end
