# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :indicator do
    total 1.5
    edition { FactoryGirl.create :edition }
    country { FactoryGirl.create :country }
  end
end
