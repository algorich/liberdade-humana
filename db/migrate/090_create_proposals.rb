class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.text :content
      t.references :proponent
      t.references :edition
      t.timestamps
    end
  end
end
