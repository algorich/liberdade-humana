class AddCodeToCountries < ActiveRecord::Migration
  def up
    add_column :countries, :code, :string
    add_index :countries, :code
  end

  def down
    remove_index :countries, :code
    remove_column :countries, :code
  end
end
