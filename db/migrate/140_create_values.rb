class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
      t.references :edition
      t.references :country

      t.float :total
      t.text :values

      t.timestamps
    end
  end
end
