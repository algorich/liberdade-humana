class Value < ActiveRecord::Base
  attr_accessible :edition, :edition_id, :country, :country_id, :total, :values

  belongs_to :edition
  belongs_to :country

  validates_presence_of :edition_id, :country_id

end
