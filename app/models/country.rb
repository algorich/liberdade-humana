class Country < ActiveRecord::Base
  attr_accessible :continent, :name, :labels, :label_ids, :code

  has_many :values, dependent: :destroy
  has_and_belongs_to_many :labels

  validates :name, presence: true
end
