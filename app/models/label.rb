class Label < ActiveRecord::Base
  attr_accessible :name, :countries

  has_and_belongs_to_many :countries

  validates :name, presence: true
end
