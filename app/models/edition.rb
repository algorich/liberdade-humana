class Edition < ActiveRecord::Base
  attr_accessible :name, :node_fake, :values, :nodes

  has_many :nodes, dependent: :destroy
  has_many :proposals, dependent: :destroy
  has_many :values, dependent: :destroy

  validates :name, presence: true

  after_save :create_nodes
  before_update :clean_nodes

  def categories
    ctgs = []

    self.nodes.each do |n|
      ctgs << n.category
    end

    return ctgs
  end

  def clean_nodes
    Node.where('edition_id = ?', self.id).delete_all
  end

  def create_nodes
    tree, nodes = nil, []
    nodes_array = ActiveSupport::JSON.decode(self.node_fake)

    return if nodes_array.blank?

    nodes_array.each do |n|
      category = Category.find_by_id(n[1])

      nodes << [n[0], category] unless category.nil?
    end

    nodes = organize(nodes, 0)

    create_node_tree(nil, nodes) unless nodes.empty?
  end

  def create_node_tree parent, nodes
    nodes.each do |elem|
      unless parent.nil?
        node_parent = parent.children.create(edition: self,
          category: elem.keys.first[1])
      else
        node_parent = Node.create(edition: self, category: elem.keys.first[1])
      end

      create_node_tree(node_parent, elem[elem.keys.first])
    end
  end

  def separate vet, pos
    i, last, ret = 1, nil, [[]]

    vet.each do |elem|
      unless (val = elem[0].split('.')[pos].to_i) == i
        if ((! last.nil? && val == 1) || (! last.nil? && (val - last == 1)))
          ret << []
          i += 1
        else
          return []
        end
      end

      last = val
      ret[i - 1] << elem
    end

    return vet.empty? ? [] : ret
  end

  def link vet
    ret = []

    vet.each do |elem|
      ret << { elem.shift => elem.clone }
    end

    return ret
  end

  def organize vet, pos
    ret = []

    unless vet.empty?
      (ret = link(separate(vet, pos))).each_with_index do |elem, i|
        ret[i][elem.keys.first] = organize(elem[elem.keys.first], pos + 1)
      end
    end

    return ret
  end

end
