# -*- encoding : utf-8 -*-
class Configuration < ActiveRecord::Base

  attr_accessible :email, :keywords, :description, :google_analytics

  validates_presence_of :email
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  rails_admin do
    edit do
      group :email do
        label 'Configurações de email'
        field :email
      end

      group :info_search do
        label 'Informações para buscadores'

        field :keywords do
           help 'Separadas por vírgula. Recomendável no máximo 10 palavras chave.'
        end
        field :description do
          help 'Descrição utilizada pelos buscadores. Recomendável até 160 caracteres.'
        end

        field :google_analytics
      end
    end
  end
end

