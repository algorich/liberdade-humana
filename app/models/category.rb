class Category < ActiveRecord::Base
  attr_accessible :name, :nodes, :values

  has_many :nodes, dependent: :destroy

  validates :name, presence: true

  def editions
    edts = []

    Edition.all.each do |e|
      edts << e if e.categories.include? self
    end

    return edts
  end

end
