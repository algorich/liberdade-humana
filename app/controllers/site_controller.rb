# -*- encoding : utf-8 -*-
class SiteController < ApplicationController

  before_filter Proc.new {
    @last_edition = Edition.last
    @penultimate_edition_values = Edition.last(2).first.values
    @countries = Country.order('name ASC')
  }

  def index
    @values = @last_edition.values.order('total DESC')
  end

  def ranking
    @values = @last_edition.values.order('total DESC')
  end

  def country
    @countries.each do |c|
      @country = c if c.name.parameterize('-') == params[:name]
    end

    unless @country.nil?
      @values = @country.values.includes(:edition).
        order('editions.created_at ASC')
      @editions = Edition.order('created_at ASC')
    else
      raise ActionController::RoutingError
    end

  end

  # CONTACT BEGIN

  def contact
    @contact = Page.where('indicator = ?', Page::PAGES[:contact]).first
    @contact_form = Contact.new

    raise ActionController::RoutingError unless @contact.published
  end

  def send_contact
    @contact = Page.where('indicator = ?', Page::PAGES[:contact]).first
    @contact_form = Contact.new(params[:contact])

    if @contact_form.save
      redirect_to(contact_path, :notice => "Mensagem enviada com sucesso.")
    else
      render :action => :contact
    end
  end

  # CONTACT END
end
