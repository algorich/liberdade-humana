//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery.select2.min
//= require jquery-jvectormap-1.2.2.min
//= require jquery-jvectormap-world-mill-en
//= require bootstrap

$(function () {
    $('.chzn-select').select2();
});
