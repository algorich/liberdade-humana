module MapHelper
  def image_map_tag(country)
    src = "http://maps.googleapis.com/maps/api/staticmap?center=#{country}&zoom=5&size=221x147&maptype=terrain&sensor=false"

    %{ <img src="#{src}" alt="#{country}" width="221" hidth="147" class="thumbnail" /> }.html_safe
  end
end
