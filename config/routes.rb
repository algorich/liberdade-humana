# -*- encoding : utf-8 -*-

LiberdadeHumana::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  # 1. user routes
  devise_for :users

  # 2. contact routes
  get '/contact' => 'site#contact', as: :contact
  post '/contact' => 'site#send_contact', as: :send_contact

  get '/ranking' => 'site#ranking', as: :ranking
  get '/pais/:name' => 'site#country', as: :country

  root :to => 'site#index'
end
