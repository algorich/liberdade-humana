## -*- encoding : utf-8 -*-
#
#RailsAdmin.config do |config|
#  config.main_app_name = [ 'Liberdade Humana', 'Administração' ]
#
#  config.current_user_method { current_user } #auto-generated
#  config.yell_for_non_accessible_fields = false
#  config.authorize_with :cancan
#
#  config.actions do
#    # root actions
#    dashboard                     # mandatory
#    # collection actions
#    index                         # mandatory
#    new
#    export
#    history_index
#    bulk_delete
#    # member actions
#    show
#    edit
#    delete
#
#    history_show
#    show_in_app
#
#    # Add the nestable action for each model
#    # nestable do
#    #   visible do
#    #     %w(Category TreelessCategory).include? bindings[:abstract_model].model_name
#    #   end
#    # end
#  end
#
#
####
#### Humam freedom data
####
#
#  config.model Value do
#    navigation_label 'Índice'
#    weight 1
#
#    list do
#      field :edition
#      field :country
#      field :total
#    end
#
#    edit do
#      field :edition
#      field :country
#      field :total
#
#      field :values do
#        partial :values
#      end
#    end
#  end
#
#  config.model Edition do
#    navigation_label 'Índice'
#    weight 2
#
#    list do
#      field :name
#    end
#
#    edit do
#      field :name
#
#      field :node_fake do
#        partial :node_fake
#      end
#    end
#  end
#
#  config.model Category do
#    navigation_label 'Índice'
#    weight 3
#
#    list do
#      field :name
#    end
#
#    edit do
#      field :name
#    end
#  end
#
#  config.model Country do
#    navigation_label 'Índice'
#    weight 4
#
#    list do
#      field :name
#    end
#
#    edit do
#      field :name
#      field :labels
#    end
#  end
#
#  config.model Configuration do
#    visible false
#  end
#
#  config.model Label do
#    visible false
#  end
#
#  config.model Node do
#    visible false
#  end
#
#  config.model Page do
#    visible false
#  end
#
#  config.model Proponent do
#    visible false
#  end
#
#  config.model Proposal do
#    visible false
#  end
#
#end
